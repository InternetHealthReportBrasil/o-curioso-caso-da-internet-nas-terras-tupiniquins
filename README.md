## **O que é o IHR** 

**O Internet Health Report** [[1](https://internethealthreport.org/v01/)][[4](https://internethealthreport.org/2017/)][[5](https://internethealthreport.org/2018/)][[6](https://internethealthreport.org/2019/)] é uma iniciativa de fonte aberta, para documentar e explicar o que está acontecendo com a saúde da Internet. Se trata de uma compilação de pesquisas que explicam o que está saudável e o que está ferindo a Internet, desde uma perspectiva pessoal à preocupações globais a partir de 5 tópicos principais: **Segurança & Privacidade, Abertura, Inclusão Digital, Alfabetização Web e Descentralização**. Pesquisas, histórias e comentários públicos de várias partes do mundo contribuem para este relatório anual.

## **IHR Brasil**

O `Relatório da Saúde da Internet ˆBrasileira` é baseado nos esforços feitos para realizar o [`Internet Health Report`](https://internethealthreport.org/2019/). Entendemos que a internet é um recurso global que está em constante ameaça e precisamos unir forças para torná-la um espaço aberto e inclusivo para todas(os).

A ideia de  desenvolver um relatório sobre a saúde da internet nas terras brasileiras, começou quando a primeira versão do [Internet Health Report](https://internethealthreport.org/v01/) foi lançado em 2017. Notamos que pensar somente nos aspectos globais não responde às inquietudes do que acontece localmente, ainda mais se concentrarmos nas singularidades da América Latina, ou ainda, focando no Brasil, um país com dimensões continentais.

O que queremos ao criar **`O curioso caso da Internet nas Terras Tupiniquins: a situação brasileira v.01`**, é gerar um relatório centrado nos aspectos globais, mas que acima de tudo, faça perguntas sobre as especificidades brasileiras. 

Identificamos a necessidade de representatividade local em questões que dizem respeito às condições sociais, econômicas e culturais de cada uma das regiões que fazem parte do nosso país. É importante destacar que o Brasil é um país de dimensões continentais e isso significa não apenas uma, mas peculiaridades distintas de cada região. 

A idéia central é possibilitar a visualização dos problemas gerais, localizando-os geograficamente e por tópico: Privacidade & Segurança, Abertura, Alfabetização Web, Inclusão Digital e Descentralização. Dessa forma, possibilita-se unir esforços para obter soluções mais eficazes para tornar a Internet mais saudável e, ao mesmo tempo, identificar, visualizar, fortalecer alianças, criar estratégias e ações.
Como estamos criando o IHR Brasil

O curioso caso da Internet nas Terras Tupiniquins: a situação brasileira v.01 é um projeto aberto, hospedado no GitLab[[2](https://gitlab.com/InternetHealthReportBrasil/o-curioso-caso-da-internet-nas-terras-tupiniquins)], com postagens a serem feitas no Global Voices[[3](https://globalvoices.org/)].

Acreditamos que é fundamental valorizar o trabalho já realizado por pessoas, grupos, projetos e incluir essas vozes no relatório, por isso começamos de onde já conhecemos para daí expandir nossas pesquisas. Neste primeiro momento, é essencial buscar pessoas, projetos, comunidades e grupos que estejam trabalhando para tecnologias que atendam à sociedade e, claro, à saúde da internet.

Próximo passo, será abrir um chamado para colaboração, um sprint, onde pessoas possam compartilhar casos, sugestões, textos, notas e idéias para adicionar ao capítulo brasileiro. Trabalharemos para alcançar colaboradores de todos os cantos do Brasil.

Avançaremos com entrevistas à pessoas que são referências em cada uma das tópicos guias (Privacidade & Segurança, Abertura, Alfabetização Web, Inclusão Digital e Descentralização). Nos basearemos em um roteiro com algumas perguntas chaves, como por exemplo “Quais os maiores problemas enfrentados na internet hoje?; “O que está fazendo a internet pior?”; “Quais soluções propor?”; “O que está fazendo a internet melhor?; “Que projetos, grupos, coletivos, pessoas fazem parte da da saúde da internet?… 

A partir dessas respostas, iremos formando histórias, causos e narrativas que nos levarão a outras referências, e assim vai crescendo nosso relatório. Em seguida, depois de testar essa metodologia, queremos aplicá-la a nossos vizinhos latino-americanos, pois compartilhamos histórias, oportunidades e dificuldades e dilemas semelhantes. 

Sabemos que este é um trabalho de proporções épicas, mas é por isso que confiamos e nos apoiamos em um sistema aberto e colaborativo.


**Nota:**

- [1] https://internethealthreport.org/v01/ 
- [2] https://gitlab.com/InternetHealthReportBrasil/o-curioso-caso-da-internet-nas-terras-tupiniquins  
- [3] https://globalvoices.org/ 
- [4] https://internethealthreport.org/2017/ 
- [5] https://internethealthreport.org/2018/ 
- [6] https://internethealthreport.org/2019/ 
